package org.pyw.spider.entity;

import org.springframework.beans.factory.annotation.Value;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Objects;


@Entity(name = "spider_article")
public class SpiderArticleEntity {
    private int id;
    private Integer categoryId;
    private int mediaId;
    private String mediaName;
    private Integer siteId;
    private String sourceUrl;
    private Integer sourceLogId;
    private String name;
    private String subname;
    private String imageUrl;
    private String videoUrl;
    private String videoImage;
    private String videoCode;
    private String keywords;
    private String description;
    private String content;
    private Integer hits;
    private Integer digs;
    private String author;
    private String source;
    private Byte publish;
    private Byte checkStatus;
    private Date createTime;
    private Date publishTime;
    private Byte delFlag;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "category_id")
    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    @Basic
    @Column(name = "media_id")
    public int getMediaId() {
        return mediaId;
    }

    public void setMediaId(int mediaId) {
        this.mediaId = mediaId;
    }

    @Basic
    @Column(name = "media_name")
    public String getMediaName() {
        return mediaName;
    }

    public void setMediaName(String mediaName) {
        this.mediaName = mediaName;
    }

    @Basic
    @Column(name = "site_id")
    public Integer getSiteId() {
        return siteId;
    }

    public void setSiteId(Integer siteId) {
        this.siteId = siteId;
    }

    @Basic
    @Column(name = "source_url")
    public String getSourceUrl() {
        return sourceUrl;
    }

    public void setSourceUrl(String sourceUrl) {
        this.sourceUrl = sourceUrl;
    }

    @Basic
    @Column(name = "source_log_id")
    public Integer getSourceLogId() {
        return sourceLogId;
    }

    public void setSourceLogId(Integer sourceLogId) {
        this.sourceLogId = sourceLogId;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "subname")
    public String getSubname() {
        return subname;
    }

    public void setSubname(String subname) {
        this.subname = subname;
    }

    @Basic
    @Column(name = "image_url")
    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @Basic
    @Column(name = "video_url")
    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    @Basic
    @Column(name = "video_image")
    public String getVideoImage() {
        return videoImage;
    }

    public void setVideoImage(String videoImage) {
        this.videoImage = videoImage;
    }

    @Basic
    @Column(name = "video_code")
    public String getVideoCode() {
        return videoCode;
    }

    public void setVideoCode(String videoCode) {
        this.videoCode = videoCode;
    }

    @Basic
    @Column(name = "keywords")
    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    @Basic
    @Column(name = "description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "content")
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Basic
    @Column(name = "hits")
    public Integer getHits() {
        return hits;
    }

    public void setHits(Integer hits) {
        this.hits = hits;
    }

    @Basic
    @Column(name = "digs")
    public Integer getDigs() {
        return digs;
    }

    public void setDigs(Integer digs) {
        this.digs = digs;
    }

    @Basic
    @Column(name = "author")
    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    @Basic
    @Column(name = "source")
    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    @Basic
    @Column(name = "publish")
    @Value("1")
    public Byte getPublish() {
        return publish;
    }

    public void setPublish(Byte publish) {
        this.publish = publish;
    }

    @Basic
    @Column(name = "check_status")
    @Value("0")
    public Byte getCheckStatus() {
        return checkStatus;
    }

    public void setCheckStatus(Byte checkStatus) {
        this.checkStatus = checkStatus;
    }

    @Basic
    @Column(name = "create_time")
    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Basic
    @Column(name = "publish_time")
    public Date getPublishTime() {
        return publishTime;
    }

    public void setPublishTime(Date publishTime) {
        this.publishTime = publishTime;
    }

    @Basic
    @Column(name = "del_flag")
    @Value("0")
    public Byte getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(Byte delFlag) {
        this.delFlag = delFlag;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SpiderArticleEntity that = (SpiderArticleEntity) o;
        return id == that.id &&
                mediaId == that.mediaId &&
                Objects.equals(categoryId, that.categoryId) &&
                Objects.equals(mediaName, that.mediaName) &&
                Objects.equals(siteId, that.siteId) &&
                Objects.equals(sourceUrl, that.sourceUrl) &&
                Objects.equals(sourceLogId, that.sourceLogId) &&
                Objects.equals(name, that.name) &&
                Objects.equals(subname, that.subname) &&
                Objects.equals(imageUrl, that.imageUrl) &&
                Objects.equals(videoUrl, that.videoUrl) &&
                Objects.equals(videoImage, that.videoImage) &&
                Objects.equals(videoCode, that.videoCode) &&
                Objects.equals(keywords, that.keywords) &&
                Objects.equals(description, that.description) &&
                Objects.equals(content, that.content) &&
                Objects.equals(hits, that.hits) &&
                Objects.equals(digs, that.digs) &&
                Objects.equals(author, that.author) &&
                Objects.equals(source, that.source) &&
                Objects.equals(publish, that.publish) &&
                Objects.equals(checkStatus, that.checkStatus) &&
                Objects.equals(createTime, that.createTime) &&
                Objects.equals(publishTime, that.publishTime) &&
                Objects.equals(delFlag, that.delFlag);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, categoryId, mediaId, mediaName, siteId, sourceUrl, sourceLogId, name, subname, imageUrl, videoUrl, videoImage, videoCode, keywords, description, content, hits, digs, author, source, publish, checkStatus, createTime, publishTime, delFlag);
    }
}
