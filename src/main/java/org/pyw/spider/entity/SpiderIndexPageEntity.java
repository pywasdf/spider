package org.pyw.spider.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Objects;




@Entity(name = "spider_index_page")
public class SpiderIndexPageEntity  implements Serializable {
    private static final long serialVersionUID = 1L;
    private Long id;
    private String url;
    private String method;
    private String pageParamName;
    private Byte type;
    private String indexExpression;
    private String newsExpression;
    private Integer siteId;
    private Integer categoryId;
    private Timestamp createTime;
    private Date lastDate;
    private String categoryName;
    private Integer delFlag;
    private Integer isOpen;

    public Integer getIsOpen() {
        return isOpen;
    }

    public void setIsOpen(Integer isOpen) {
        this.isOpen = isOpen;
    }

    public void setLastDate(Timestamp lastDate) {
        this.lastDate = lastDate;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Id
    @Column(name = "id")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "url")
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Basic
    @Column(name = "method")
    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    @Basic
    @Column(name = "page_param_name")
    public String getPageParamName() {
        return pageParamName;
    }

    public void setPageParamName(String pageParamName) {
        this.pageParamName = pageParamName;
    }

    @Basic
    @Column(name = "type")
    public Byte getType() {
        return type;
    }

    public void setType(Byte type) {
        this.type = type;
    }

    @Basic
    @Column(name = "index_expression")
    public String getIndexExpression() {
        return indexExpression;
    }

    public void setIndexExpression(String indexExpression) {
        this.indexExpression = indexExpression;
    }

    @Basic
    @Column(name = "news_expression")
    public String getNewsExpression() {
        return newsExpression;
    }

    public void setNewsExpression(String newsExpression) {
        this.newsExpression = newsExpression;
    }

    @Basic
    @Column(name = "site_id")
    public Integer getSiteId() {
        return siteId;
    }

    public void setSiteId(Integer siteId) {
        this.siteId = siteId;
    }

    @Basic
    @Column(name = "category_id")
    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    @Basic
    @Column(name = "create_time")
    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SpiderIndexPageEntity that = (SpiderIndexPageEntity) o;
        return id == that.id &&
                Objects.equals(url, that.url) &&
                Objects.equals(method, that.method) &&
                Objects.equals(pageParamName, that.pageParamName) &&
                Objects.equals(type, that.type) &&
                Objects.equals(indexExpression, that.indexExpression) &&
                Objects.equals(newsExpression, that.newsExpression) &&
                Objects.equals(siteId, that.siteId) &&
                Objects.equals(categoryId, that.categoryId) &&
                Objects.equals(createTime, that.createTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, url, method, pageParamName, type, indexExpression, newsExpression, siteId, categoryId, createTime);
    }

    @Basic
    @Column(name = "last_date")
    public Date getLastDate() {
        return lastDate;
    }

    public void setLastDate(Date lastDate) {
        this.lastDate = lastDate;
    }

    @Basic
    @Column(name = "category_name")
    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    @Basic
    @Column(name = "del_flag")
    public Integer getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(Integer delFlag) {
        this.delFlag = delFlag;
    }
}
