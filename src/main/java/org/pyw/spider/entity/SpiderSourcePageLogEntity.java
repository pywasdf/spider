package org.pyw.spider.entity;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Objects;




@Entity(name = "spider_source_page_log")
public class SpiderSourcePageLogEntity {
    private Long id;
    private Long indexPageId;
    private String url;
    private String html;
    private String newsExpression;
    private Integer categoryId;
    private String splidCategoryName;
    private Byte status;
    private Date createTime;
    private Date updateTime;
    private String errMessage;

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    public void setUpdateTime(Timestamp updateTime) {
        this.updateTime = updateTime;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Id
    @Column(name = "id")
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "index_page_id")
    public Long getIndexPageId() {
        return indexPageId;
    }

    public void setIndexPageId(Long indexPageId) {
        this.indexPageId = indexPageId;
    }

    @Basic
    @Column(name = "url")
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Basic
    @Column(name = "html")
    public String getHtml() {
        return html;
    }

    public void setHtml(String html) {
        this.html = html;
    }

    @Basic
    @Column(name = "news_expression")
    public String getNewsExpression() {
        return newsExpression;
    }

    public void setNewsExpression(String newsExpression) {
        this.newsExpression = newsExpression;
    }

    @Basic
    @Column(name = "category_id")
    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    @Basic
    @Column(name = "splid_category_name")
    public String getSplidCategoryName() {
        return splidCategoryName;
    }

    public void setSplidCategoryName(String splidCategoryName) {
        this.splidCategoryName = splidCategoryName;
    }

    @Basic
    @Column(name = "status")
    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SpiderSourcePageLogEntity that = (SpiderSourcePageLogEntity) o;
        return id == that.id &&
                Objects.equals(indexPageId, that.indexPageId) &&
                Objects.equals(url, that.url) &&
                Objects.equals(html, that.html) &&
                Objects.equals(newsExpression, that.newsExpression) &&
                Objects.equals(categoryId, that.categoryId) &&
                Objects.equals(splidCategoryName, that.splidCategoryName) &&
                Objects.equals(status, that.status);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, indexPageId, url, html, newsExpression, categoryId, splidCategoryName, status);
    }

    @Basic
    @Column(name = "create_time")
    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Basic
    @Column(name = "update_time")
    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Basic
    @Column(name = "err_message")
    public String getErrMessage() {
        return errMessage;
    }

    public void setErrMessage(String errMessage) {
        this.errMessage = errMessage;
    }
}
