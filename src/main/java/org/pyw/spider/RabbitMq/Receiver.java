package org.pyw.spider.RabbitMq;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import org.pyw.spider.dao.SpiderIndexPageRespository;
import org.pyw.spider.entity.SpiderIndexPageEntity;
import org.pyw.spider.service.SpiderService;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@RabbitListener(queues="spider")    //监听器监听指定的
public class Receiver {
    @Autowired
    private SpiderService spiderService;

    @RabbitHandler
    public void processC(String  strJsonSpiderIndexPage) {
        System.out.println(strJsonSpiderIndexPage);
        //SpiderIndexPageEntity spiderIndexPageEntity= JSONObject.parseObject(strJsonSpiderIndexPage,new TypeReference<SpiderIndexPageEntity>() {});
        //spiderService.saveSpiderSourceLog(spiderIndexPageEntity);
    }

}
