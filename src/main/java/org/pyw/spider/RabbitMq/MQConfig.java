package org.pyw.spider.RabbitMq;

import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class MQConfig {
    @Bean
    public Queue queue() {
        return new Queue("spider");//,false,false, false, args);
    }
    @Bean
    public Queue queueNews() {
        return new Queue("news");//,false,false, false, args);
    }
}
