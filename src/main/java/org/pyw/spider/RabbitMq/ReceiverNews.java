package org.pyw.spider.RabbitMq;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import org.pyw.spider.entity.SpiderSourcePageLogEntity;
import org.pyw.spider.service.SpiderService;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@RabbitListener(queues="news")    //监听器监听指定的
public class ReceiverNews {
    @Autowired
    private SpiderService spiderService;

    @RabbitHandler
    public void processC(String  strJsonSpiderIndexPage) {
        System.out.println(strJsonSpiderIndexPage);
        SpiderSourcePageLogEntity spiderSourcePageLogEntity= JSONObject.parseObject(strJsonSpiderIndexPage,new TypeReference<SpiderSourcePageLogEntity>() {});
        spiderService.saveSpiderNews(spiderSourcePageLogEntity);
    }

}
