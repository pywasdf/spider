package org.pyw.spider.RabbitMq;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import org.pyw.spider.entity.SpiderIndexPageEntity;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Producer {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    public void send(String qeueName,String strJsonSpiderIndexPage) {
        rabbitTemplate.convertAndSend(qeueName, strJsonSpiderIndexPage);
    }

}
