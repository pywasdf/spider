package org.pyw.spider.service;

import org.pyw.spider.entity.SpiderIndexPageEntity;
import org.pyw.spider.entity.SpiderSourcePageLogEntity;

import java.util.List;
import java.util.Map;

public interface SpiderService {
    /**
     * 保存处理需要抓取的新闻
     * @return
     */
    void saveSpiderSourceLog();
    void saveSpiderSourceLog(SpiderIndexPageEntity spiderIndexPageEntity);
    /**
     * 处理抓取的新闻
     */
    void saveSpiderNews();
    void saveSpiderNews(SpiderSourcePageLogEntity spiderSourcePageLogEntity);

}
