package org.pyw.spider.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.pyw.spider.dao.SpiderArticleRepository;
import org.pyw.spider.dao.SpiderIndexPageRespository;
import org.pyw.spider.dao.SpiderSourcePageLogRepository;
import org.pyw.spider.entity.SpiderArticleEntity;
import org.pyw.spider.entity.SpiderIndexPageEntity;
import org.pyw.spider.entity.SpiderSourcePageLogEntity;
import org.pyw.spider.service.SpiderService;
import org.pyw.spider.utils.EmojiConverterUtil;
import org.pyw.spider.utils.Tools;
import org.pyw.spider.utils.spiderUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.ParseException;
import java.util.*;

@Service
public class HuoXingSpiderImpl implements SpiderService {

    @Autowired
    private SpiderArticleRepository spiderArticleRepository;
    @Autowired
    private SpiderIndexPageRespository spiderIndexPageRespository;

    @Autowired
    private SpiderSourcePageLogRepository spiderSourcePageLogRepository;


    @Override
    public void saveSpiderSourceLog() {
        List<SpiderIndexPageEntity> list = spiderIndexPageRespository.findByIsOpen(1);

        for (SpiderIndexPageEntity item : list) {
            saveSpiderSourceLog(item);
        }


    }

    /**
     * 按照索引记录来抓取数据
     *
     * @param spiderIndexPageEntity
     */
    public void saveSpiderSourceLog(SpiderIndexPageEntity spiderIndexPageEntity) {
        /******************************获取该条记录的详细字段值****************************************/
        String listUrl = spiderIndexPageEntity.getUrl();//获取URL
        String method = spiderIndexPageEntity.getMethod();//获取请求方式
        Byte type=spiderIndexPageEntity.getType();
        Map ruleMap = (Map) JSONObject.parseObject(spiderIndexPageEntity.getIndexExpression());//获取索引页面的正则
        Map newsRuleMap = (Map) JSONObject.parseObject(spiderIndexPageEntity.getNewsExpression());//

        List<Map<String, String>> retList = new ArrayList<>();
        Integer pageNum = 1;
        boolean indexFlag = true;

        while (indexFlag) {
            //获取页面请求参数
            Map paramsMap = (Map) JSONObject.parseObject(spiderIndexPageEntity.getPageParamName().replace("$page", pageNum.toString()));

            retList = spiderUtil.getIndexList(listUrl, method,type, paramsMap, ruleMap);
            pageNum++;

            if (retList.size() <= 0) {
                Sort sort = new Sort(Sort.Direction.DESC, "createTime");
                Pageable pageable = new PageRequest(0, 1, sort);
                Page<SpiderSourcePageLogEntity> lastNewsList = spiderSourcePageLogRepository.findByStatus((byte) 1, pageable);

                if (lastNewsList.getSize() > 0) {
                    spiderIndexPageEntity.setLastDate(lastNewsList.getContent().get(0).getUpdateTime());
                    spiderIndexPageRespository.save(spiderIndexPageEntity);
                    indexFlag = false;
                }

            }


            for (Map<String, String> map : retList) {
                String newsUrl = map.get("url").toString();
                Date createTime = null, updateTime = null;//新闻的创建日期和修改日期

                //获取新闻的创建日期
                if (map.get("createTime") != null && !map.get("createTime").equals("")) {
                    try {
                        createTime = Tools.getDate(Long.parseLong(map.get("createTime")));
                    } catch (Exception ex) {
                        try {
                            createTime = Tools.getDate(map.get("createTime"), ruleMap.get("dateFormat").toString());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
                //获取新闻的更新日期
                if (map.get("updateTime") != null && !map.get("updateTime").equals("")) {
                    try {
                        updateTime = Tools.getDate(Long.parseLong(map.get("updateTime")));
                    } catch (Exception ex) {
                        try {
                            updateTime = Tools.getDate(map.get("updateTime"), ruleMap.get("dateFormat").toString());
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }
                }


                if (spiderSourcePageLogRepository.findByUrl(newsUrl).size() > 0) {
                    indexFlag = false;
                    break;
                } else {
                    try {
                        SpiderSourcePageLogEntity spiderSourcePageLogEntity = new SpiderSourcePageLogEntity();
                        spiderSourcePageLogEntity.setCategoryId(spiderIndexPageEntity.getCategoryId());
                        spiderSourcePageLogEntity.setNewsExpression(spiderIndexPageEntity.getNewsExpression());
                        spiderSourcePageLogEntity.setIndexPageId(spiderIndexPageEntity.getId());
                        spiderSourcePageLogEntity.setUrl(newsUrl);
                        spiderSourcePageLogEntity.setStatus((byte) 0);
                        if (createTime != null)
                            spiderSourcePageLogEntity.setCreateTime(createTime);
                        if (updateTime != null)
                            spiderSourcePageLogEntity.setUpdateTime(updateTime);

                        spiderSourcePageLogRepository.save(spiderSourcePageLogEntity);
                    } catch (Exception e) {

                    }

                }
            }

        }


    }

    /**
     * 处理所有状态为0的日志
     */
    @Override
    public void saveSpiderNews() {

        Sort sort = new Sort(Sort.Direction.DESC, "status");
        Pageable pageable = new PageRequest(0, 10000, sort);
        Page<SpiderSourcePageLogEntity> list = spiderSourcePageLogRepository.findByStatus((byte) 0, pageable);

        for (SpiderSourcePageLogEntity item : list) {

            try {
                saveSpiderNews(item);
            } catch (Exception ex) {
                item.setErrMessage(ex.getMessage());
                spiderSourcePageLogRepository.save(item);
            }

        }
    }

    @Override
    public void saveSpiderNews(SpiderSourcePageLogEntity spiderSourcePageLogEntity) {

        try {
            String strNewsURL = spiderSourcePageLogEntity.getUrl();
            Map newsRuleMap = (Map) JSONObject.parseObject(spiderSourcePageLogEntity.getNewsExpression());//获取详情页正则
            Map<String, String> retMap = spiderUtil.getContent(strNewsURL, newsRuleMap);

            SpiderArticleEntity spiderArticleEntity = JSON.parseObject(JSONObject.toJSONString(retMap), SpiderArticleEntity.class);
            spiderArticleEntity.setSourceUrl(strNewsURL);
            spiderArticleEntity.setPublish((byte) 1);
            spiderArticleEntity.setCreateTime(spiderSourcePageLogEntity.getCreateTime());
            spiderArticleEntity.setPublishTime(spiderSourcePageLogEntity.getUpdateTime());
            spiderArticleEntity.setCheckStatus((byte) 0);
            spiderArticleEntity.setDelFlag((byte) 0);
            spiderArticleEntity.setContent(EmojiConverterUtil.emojiConvert1(spiderArticleEntity.getContent()));


            List<SpiderArticleEntity> retList = spiderArticleRepository.findBysourceUrl(strNewsURL);
            if (retList.size() <= 0) {
                String source = spiderArticleEntity.getSource();

                spiderArticleRepository.save(spiderArticleEntity);
                spiderSourcePageLogEntity.setStatus((byte) 1);
                spiderSourcePageLogRepository.save(spiderSourcePageLogEntity);
            } else {
                spiderSourcePageLogEntity.setStatus((byte) 1);
                spiderSourcePageLogRepository.save(spiderSourcePageLogEntity);
            }
        } catch (Exception ex) {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            try
            {
                ex.printStackTrace(pw);
            } finally
            {
                pw.close();
            }
            spiderSourcePageLogEntity.setErrMessage(sw.getBuffer().toString());
            spiderSourcePageLogRepository.save(spiderSourcePageLogEntity);
        }
    }
}
