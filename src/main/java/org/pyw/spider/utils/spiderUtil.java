package org.pyw.spider.utils;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.JSONPath;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.*;

public class spiderUtil {
    /**
     * 根据请求的URL和页面抓取规则抓取数据
     *
     * @param url
     * @param ruleMap
     */
    public static Map<String, String> getContent(String url, Map<String, String> ruleMap) {
        Map<String, String> retMap = new HashMap<>();

        try {
            Document doc = Jsoup.connect(url).timeout(50000).get();
            for (String key : ruleMap.keySet()) {
                String rule = ruleMap.get(key).equals("") ? "--" : ruleMap.get(key);
                retMap.put(key, "");
                if (!rule.equals("--")) {
                    Elements elements = doc.select(rule);
                    if (elements.size() > 0) {
                        if (key.equals("content"))
                            retMap.put(key, elements.get(0).html());
                        else if(key.equals("nextPageURL"))
                        {
                            ;
                            retMap.put("nextPageURL",elements.get(0).attr("href"));
                        }
                        else
                            retMap.put(key, elements.get(0).text());
                    }
//                    else {
//                        retMap.put("status", "error");
//                        retMap.put("errorkey", key);
//                        retMap.put("url", url);
//                        return retMap;
//                    }
                }
            }
            System.out.println("as");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return retMap;
        }
    }


    /**
     * 抓取索引页面
     * @param url
     * @param Method
     * @param paramsMap
     * @param ruleMap
     * @return
     */
    public static List<Map<String, String>> getIndexList(String url,String Method,Byte Type, Map<String, String> paramsMap, Map<String, String> ruleMap) {
        List<Map<String, String>> retList = new ArrayList<>();
        String strRet = JsoupHelper.execute(url, Method,paramsMap, "utf-8", null);

        String listPathRule = ruleMap.containsKey("listPath") ? ruleMap.get("listPath") : "--";

        if (!listPathRule.equals("--")) {
            String strJson = JSONPath.read(strRet, listPathRule).toString();
            JSONArray jsonArray = JSONArray.parseArray(strJson);
            for (Integer i = 0; i < jsonArray.size(); i++) {
                Map<String, String> tempMap = new HashMap<>();

                if (Type == 2){
                //获取新闻链接
                String id = JSONPath.read(jsonArray.getJSONObject(i).toJSONString(), ruleMap.get("id").toString()).toString();
                String tempURL = ruleMap.get("url").replace("$id", id);
                tempMap.put("url", tempURL);

                //获取createTime
                Object obj_createTime = JSONPath.read(jsonArray.getJSONObject(i).toJSONString(), ruleMap.get("createTime").toString());
                tempMap.put("createTime", obj_createTime == null ? null : obj_createTime.toString());
                Object obj_updateTime = JSONPath.read(jsonArray.getJSONObject(i).toJSONString(), ruleMap.get("updateTime").toString());
                tempMap.put("updateTime", obj_updateTime == null ? null : obj_updateTime.toString());

                retList.add(tempMap);
            }
            }
        }
        return retList;
    }


    public static List<Map<String, String>> getIndexList1(String url,String Method,Map<String, String> params, Map<String, String> ruleMap) {

        List<Map<String, String>> retList = new ArrayList<>();
        try {
            String retStr = JsoupHelper.execute(url,Method,params,"utf-8",null);
            String listPathRule = ruleMap.containsKey("listPath") ? ruleMap.get("listPath") : "--";
            String strJson = JSONPath.read(retStr, listPathRule).toString();
            Document doc = Jsoup.parseBodyFragment(strJson);
            Element body = doc.body();
            Elements elements=body.select(ruleMap.get("node").toString());
            for(Element element :elements)
            {
                String tempURL = element.attr("href");
                Map<String, String> tempMap = new HashMap<>();
                tempMap.put("url", tempURL);
                retList.add(tempMap);
            }
            System.out.println("1");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return retList;
    }
}
