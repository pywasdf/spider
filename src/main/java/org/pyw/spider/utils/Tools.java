package org.pyw.spider.utils;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Tools {
    /**
     * 获取时间
     *
     * @param timestamp
     * @return
     */
    public static Date getDate(Long timestamp) {
        Calendar can = Calendar.getInstance();
        if (timestamp.toString().length() < 13)
            can.setTimeInMillis(timestamp * 1000);
        else
            can.setTimeInMillis(timestamp);
//        SimpleDateFormat fmt=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
//        System.out.println(fmt.format(can.getTime()));
//        Date a=can.getTime();
        return can.getTime();
    }

    public static Date getDate(String strDate, String dateFormat) throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat);
        Date date = simpleDateFormat.parse(strDate);
        return date;
    }


}
