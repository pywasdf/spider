package org.pyw.spider.task;

import com.alibaba.fastjson.JSONObject;
import org.pyw.spider.RabbitMq.Producer;
import org.pyw.spider.dao.SpiderIndexPageRespository;
import org.pyw.spider.dao.SpiderSourcePageLogRepository;
import org.pyw.spider.entity.SpiderIndexPageEntity;
import org.pyw.spider.entity.SpiderSourcePageLogEntity;
import org.pyw.spider.service.SpiderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class Jobs {

    @Autowired
    private SpiderIndexPageRespository spiderIndexPageRespository;
    @Autowired
    private Producer producer;
    @Value("${rabbitMqIsOpen}")
    private String rabbitMqIsOpen;

    @Autowired
    private SpiderService spiderService;
    @Autowired
    private SpiderSourcePageLogRepository spiderSourcePageLogRepository;

    @Scheduled(cron = "0 0 * * * ?")
    public void cronJobRabbitMq() {
        if (rabbitMqIsOpen.equals("true"))//如果开启Mq
        {
            List<SpiderIndexPageEntity> list = spiderIndexPageRespository.findAll();
            for (SpiderIndexPageEntity item : list) {
                producer.send("spider", JSONObject.toJSONString(item));
            }

        }
        else
        {
            spiderService.saveSpiderSourceLog();
        }

    }

    @Scheduled(cron = "0 */1 * * * ?")
    public void cronJobNews() {
        if (rabbitMqIsOpen.equals("true"))//如果开启Mq
        {
            Sort sort = new Sort(Sort.Direction.DESC, "status");
            Pageable pageable = new PageRequest(0, 10000, sort);
            Page<SpiderSourcePageLogEntity> list = spiderSourcePageLogRepository.findByStatus((byte) 0, pageable);

            for (SpiderSourcePageLogEntity item : list) {

                producer.send("news", JSONObject.toJSONString(item));

            }
        }
        else
        {
            spiderService.saveSpiderNews();
        }
    }
}
