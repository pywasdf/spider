package org.pyw.spider.dao;

import org.pyw.spider.entity.SpiderArticleEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface SpiderArticleRepository extends JpaRepository<SpiderArticleEntity,Long> {
    List<SpiderArticleEntity> findBysourceUrl(String sourceUrl);
}
