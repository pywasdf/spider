package org.pyw.spider.dao;

import org.pyw.spider.entity.SpiderArticleEntity;
import org.pyw.spider.entity.SpiderSourcePageLogEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface SpiderSourcePageLogRepository  extends CrudRepository<SpiderSourcePageLogEntity,Long> {
    /**
     * 根据URL获取
     * @param url
     * @return
     */
    List<SpiderSourcePageLogEntity> findByUrl(String url);

    Page<SpiderSourcePageLogEntity> findByStatus(Byte status, Pageable pageable);
}
