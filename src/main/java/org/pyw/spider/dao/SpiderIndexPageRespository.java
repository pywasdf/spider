package org.pyw.spider.dao;


import org.pyw.spider.entity.SpiderIndexPageEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SpiderIndexPageRespository extends JpaRepository<SpiderIndexPageEntity,Long> {
    List<SpiderIndexPageEntity> findByIsOpen(Integer isOpen);
}
